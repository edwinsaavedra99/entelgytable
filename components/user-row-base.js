
const template = document.createElement('template');
template.innerHTML = `
    <style>
    :host {
      display: contents;
    }
    td .img {
      width: 50px;
      height: 50px;
      border-radius: 50%;
      margin: 0 auto;
    }
    tr{
      
      border-bottom: 2px solid #f2f2f2;
    }
    
    .img{
      
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center center;
    }
    .table-row__see{
      width: 20px;
      padding: 8px 17px;
      display: inline-block;
      background-color: var(--primary-color);
      border-radius: 18px;
      vertical-align: middle;
      margin-right: 10px;
      cursor: pointer;
    }
    
     td {
      
      vertical-align: middle;
      text-align: center;
      padding: 10px;
        border-right: 1px solid #f8f8f8;
        font-size: 12px;
    }
    
    
    @media (max-width: 767px) {
    
       td {
          padding: 20px .625em .625em .625em;
          height: 100px;
          vertical-align: middle;
          box-sizing: border-box;
          overflow-x: hidden;
          overflow-y: auto;
          width: 120px;
          font-size: 13px;
          text-overflow: ellipsis;
      }
    
      th {
          border-bottom: 1px solid #f7f7f9;
      }
    
       tr {
          display: table-cell;
      }
    
      tr:nth-child(odd) {
          background: none;
      }
    
      tr:nth-child(even) {
          background: transparent;
      }
    
      tr td:nth-child(odd) {
          background: #F8F8F8;
          border-right: 1px solid #E6E4E4;
      }
    
      tr td:nth-child(even) {
          border-right: 1px solid #E6E4E4;
      }
    
       td {
          display: block;
          text-align: center;
      }
    }
    
    </style>
      <tr>
        <td>
            <div class="img" style="background-image: url();"></div>
        </td>
        <td id="first_name"></td>
        <td id="last_name"></td>
        <td id="email"></td>
        <td>
            <img src="./assets/icon-see-60.png" class="table-row__see"/>
        </td>
      </tr>
    `

class UserRow extends HTMLElement {
  constructor() {
    super();
    this.avatar
    this.first_name;
    this.last_name;
    this.email;
    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));

  }


  connectedCallback() {
    this.shadowRoot.querySelector(".img").style.backgroundImage = "url(" + this.getAttribute("avatar") + ")";
    this.shadowRoot.querySelector("#first_name").innerText = this.getAttribute("first_name");
    this.shadowRoot.querySelector("#last_name").innerText = this.getAttribute("last_name");
    this.shadowRoot.querySelector("#email").innerText = this.getAttribute("email");

    this.shadowRoot.querySelector(".table-row__see").addEventListener('click', () => {
      this.dispatchEvent(new Event("data", { detail: true }));
    });
  }

  disconnectedCallback() {
    try{
    this.shadowRoot.querySelector(".img").removeEventListener('click');
    } catch {}

  }

  static get observedAttributes() {
    return ['avatar', 'first_name', 'last_name', 'email'];
  }

  attributeChangedCallback(nameAtr, oldValue, newValue) {
    switch (nameAtr) {
      case "avatar":
        this.avatar = newValue;
        break;
      case "first_name":
        this.first_name = newValue;
        break;
      case "last_name":
        this.last_name = newValue;
        break;
      case "email":
        this.email = newValue;
        break;
    }
  }


}

window.customElements.define('user-row-base', UserRow);
